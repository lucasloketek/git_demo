#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#define MAX_WORD_LENGTH 100

int is_valid_word(const char *word) {
    for (int i = 0; word[i] != '\0'; i++) {
        if (!islower(word[i])) {
            return 0;
        }
    }
    return 1;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("error : please provide exactly two words\n");
        return 1;
    }

    char *hidden = argv[1];
    char *guess = argv[2];
    int hidden_len = strlen(hidden);
    int guess_len = strlen(guess);

    if (hidden_len != guess_len) {
        printf("error : hidden word is not the same length as the guess word\n");
        return 1;
    }

    if (!is_valid_word(hidden) || !is_valid_word(guess)) {
        printf("error : both words must contain only lowercase letters\n");
        return 1;
    }

    char hints[MAX_WORD_LENGTH];
    int hidden_count[26] = {0};
    int matched[hidden_len];

    for (int i = 0; i < hidden_len; i++) {
        hints[i] = 'r'; // Assume 'r' by default (red)
        matched[i] = 0;
    }
    hints[hidden_len] = '\0';

    for (int i = 0; i < hidden_len; i++) {
        if (guess[i] == hidden[i]) {
            hints[i] = 'g';
            matched[i] = 1;
        } else {
            hidden_count[hidden[i] - 'a']++;
        }
    }

    for (int i = 0; i < hidden_len; i++) {
        if (hints[i] == 'g') continue;

        int letter_index = guess[i] - 'a';
        if (hidden_count[letter_index] > 0) {
            hints[i] = 'y'; // Mark as yellow
            hidden_count[letter_index]--;
        }
    }

    printf("The hints are %s.\n", hints);
    return 0;
}
